from python:3.9

run apt-get update && apt-get install -y \
	curl \
	gcc \
	gfortran \
	make \
	libfftw3-dev \
	liblapacke-dev \
	libpng-dev \
	libopenblas-dev \
	libgl1-mesa-dev \
	xvfb \
	&& rm -rf /var/lib/apt/lists/*

workdir /opt/bart
run curl -L https://github.com/mrirecon/bart/archive/d7ab615b567728f04f1411e3d3159a10bd27d223.tar.gz \
	| tar xz --strip=1 \
	&& make shared-lib \
	&& mv libbart.so /usr/local/lib \
	&& ldconfig \
	&& make install \
	&& make allclean
env PYTHONPATH="/opt/bart/python:${PYTHONPATH}"

run curl https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py \
	| env POETRY_HOME=/opt/poetry/ python - --version 1.1.11
env PATH="/opt/poetry/bin:${PATH}"

arg NB_USER=jovyan
arg NB_UID=1000

run adduser --disabled-password --gecos "Jupyter User" --shell /bin/bash --uid $NB_UID $NB_USER
user $NB_USER

# Copy only poetry files first to avoid cache misses and poetry reruns when
# changing any other non-related files
copy --chown=$NB_USER ./pyproject.toml ./poetry.lock /home/$NB_USER/env/
workdir /home/$NB_USER/env
# --no-root: don't install the project itself (we haven't copied it yet)
run poetry install --no-root

env PYVISTA_OFF_SCREEN=true \
	PYVISTA_USE_PANEL=true \
	PYVISTA_PLOT_THEME=document \
	PYVISTA_AUTO_CLOSE=false

shell ["poetry", "run", "bash", "-c"]

run jupyter nbextension install --user --py webgui_jupyter_widgets \
	&& jupyter nbextension enable --user --py webgui_jupyter_widgets

entrypoint ["xvfb-run", "-s -screen 0 1024x768x24", "poetry", "run"]
cmd ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0"]

# Copy all files and install our library as the last step.
copy --chown=$NB_USER . /home/$NB_USER/env
run poetry install
