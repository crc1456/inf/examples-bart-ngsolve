[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/regpy/binder-ngsolve-bart/master)

- Build Docker container: `docker build .`
- Run: `docker run --name bart --publish 8888:8888 -it --rm --init $(docker build -q .)`, then copy Jupyter URL including token to browser
